License:
========
This project is heavily based on the SMPL model, that is a 3D human model that can be edited on real time. I have edited how it works for this project, which goal is to develop a 3D application for the modeling of the human body by parts of it.
Everything is explained in here. How was it made, and on what is based: https://docs.google.com/document/d/1pMXFTIkZEJ8nHBNC9c92yr0-eugMWZ8y2v7646vvdPY/edit?usp=sharing

To learn about SMPL, please visit our website: http://smpl.is.tue.mpg
You can find the SMPL paper at: http://files.is.tue.mpg.de/black/papers/SMPL2015.pdf

For comments or questions, please email me at: eriktordera@gmail.com
 

