from serialization import load_model
import numpy as np
pos = 1
mini = -4
maxi = 3

m = load_model('../models/basicmodel_f_lbs_10_207_0_v1.0.0.pkl')

m.betas[0] = 0
m.betas[1] = 0
m.betas[2] = 0.5
m.betas[3] = 0
m.betas[4] = 0
m.betas[5] = -2.3
m.betas[6] = -0.5
m.betas[7] = 2.5
m.betas[8] = 0
m.betas[9] = 0

m.betas[pos] = mini
outmesh_path = './ebermejo_outs/ebermejo_testmin.obj'

with open( outmesh_path, 'w') as fp:
    for v in m.r:
        fp.write( 'v %f %f %f\n' % ( v[0], v[1], v[2]) )

    for f in m.f+1:
        fp.write( 'f %d %d %d\n' %  (f[0], f[1], f[2]) )

print '..Output mesh saved to: ', outmesh_path


m.betas[pos] = maxi
outmesh_path = './ebermejo_outs/ebermejo_testmax.obj'

with open( outmesh_path, 'w') as fp:
    for v in m.r:
        fp.write( 'v %f %f %f\n' % ( v[0], v[1], v[2]) )

    for f in m.f+1:
        fp.write( 'f %d %d %d\n' %  (f[0], f[1], f[2]) )

print '..Output mesh saved to: ', outmesh_path
