import sys


def main():

    output = './ebermejo_outs/ebermejo_'
    v = list()

    with open(sys.argv[1], 'r') as whole:
        wholev = whole.readlines()
    whole.close()

    with open(sys.argv[2], 'r') as part:
        partv = part.readlines()
    part.close()

    with open(sys.argv[3], 'r') as boundry:
        boundryv = boundry.readlines()
    boundry.close()

    for linep in partv:
        if(linep[0] == 'v'):
            index = 0
            for linew in wholev:
                if(linew[0] == 'v'):
                    if(linew == linep):
                        stringToAppend = str(index)
                        for lineb in boundryv:
                            if(lineb[0] == 'v'):
                                if(lineb == linep):
                                    stringToAppend += " B"

                        v.append(stringToAppend)

                    index += 1


    output += sys.argv[4] + '.vertin'
    with open(output, 'w') as out:
        for vertex in v:
            out.write(str(vertex) + '\n')
    out.close()

if(len(sys.argv) == 5):
    main()
else:
    print 'need at least an input path of the whole model, a path to the target model, a path to the boundry model and a name in the format (\'sex_bodypart\\)'
