output_path = './ebermejo_outs/ebermejo_out_getdiff_m_shape_'
input_path = './ebermejo_ins/ebermejo_datasmpl_f.txt'

from serialization import load_model
import numpy as np

o = load_model( '../models/basicmodel_m_lbs_10_207_0_v1.0.0.pkl' )
d = load_model( '../models/basicmodel_m_lbs_10_207_0_v1.0.0.pkl' )



f = open(input_path, 'r')
for line in f:
    index = int(line[0])

    line = line[(line.index(' ') + 1):len(line)]
    maxi = float(line[0:line.index(' ')])

    line = line[(line.index(' ') + 1):len(line)]
    mini = float(line[0:line.index(' ')])

    line = line[(line.index(' ') + 1):len(line)]
    avg = int(line[0:len(line)])

    o.betas[index] = avg
    d.betas[index] = avg
f.close()
############################
############################
f = open(input_path, 'r')#every shape
for line in f:
    index = int(line[0])

    line = line[(line.index(' ') + 1):len(line)]
    maxi = int(line[0:line.index(' ')])

    line = line[(line.index(' ') + 1):len(line)]
    mini = int(line[0:line.index(' ')])

    line = line[(line.index(' ') + 1):len(line)]
    avg = int(line[0:len(line)])

    #generate new model and evaluate every vertex
    forl = np.arange(mini, maxi, 0.1)

    resultMatrix = list()
    for i in range(len(o.r)):
        resultMatrix.append(list())
        for j in range(len(forl)):
            resultMatrix[i].append([0, 0, 0])

    forli = 0
    for value in forl:
        d.betas[index] = value

        for i in range(len(d.r)):

            vo = o.r[i]
            vd = d.r[i]

            xs = vd[0] / vo[0]
            ys = vd[1] / vo[1]
            zs = vd[2] / vo[2]

            resultMatrix[i][forli][0] = xs
            resultMatrix[i][forli][1] = ys
            resultMatrix[i][forli][2] = zs

        forli += 1
    d.betas[index] = avg

    output = output_path + str(index) + '.shadif'
    with open(output, 'w') as fw:
        for i in range(len(resultMatrix)):
            string = ''
            for j in range(len(resultMatrix[i])):
                string += str(resultMatrix[i][j][0]) + " " + str(resultMatrix[i][j][1]) + " " + str(resultMatrix[i][j][2]) + " "
            fw.write(string + '\n')
    fw.close()

f.close()
