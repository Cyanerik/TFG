﻿using System.Collections.Generic;

public class AVL_tree
{
    public AVL_node _ROOT; 

    public AVL_tree()
    {
        _ROOT = null; 
    }

    public void insertNode(ebermejo_Vertex toInsert)
    {
        {
            if (_ROOT == null)
            {
                _ROOT = new AVL_node();
                _ROOT.key = toInsert._INDEX;
                _ROOT.data = toInsert;
            }
            else
            {
                AVL_node node = _ROOT;

                while (node != null)
                {
                    if (toInsert._INDEX < node.key)
                    {
                        AVL_node left = node._LEFT;

                        if (left == null)
                        {
                            node._LEFT = new AVL_node();
                            node._LEFT._PARENT = node;
                            node._LEFT.key = toInsert._INDEX;
                            node._LEFT.data = toInsert;

                            balanceAfterInsert(node, 1);

                            return;
                        }
                        else
                        {
                            node = left;
                        }
                    }
                    else if (toInsert._INDEX > node.key)
                    {
                        AVL_node right = node._RIGHT;

                        if (right == null)
                        {
                            node._RIGHT = new AVL_node();
                            node._RIGHT._PARENT = node;
                            node._RIGHT.key = toInsert._INDEX;
                            node._RIGHT.data = toInsert;

                            balanceAfterInsert(node, -1);

                            return;
                        }
                        else
                        {
                            node = right;
                        }
                    }
                    else
                    {
                        node.data = toInsert;

                        return;
                    }
                }
            }
        }
    }

    public void balanceAfterInsert(AVL_node node, int balance)
    {
        {
            while (node != null)
            {
                balance = (node.balance += balance);

                if (balance == 0)
                {
                    return;
                }
                else if (balance == 2)
                {
                    if (node._LEFT.balance == 1)
                    {
                        RotateRight(node);
                    }
                    else
                    {
                        RotateLeftRight(node);
                    }

                    return;
                }
                else if (balance == -2)
                {
                    if (node._RIGHT.balance == -1)
                    {
                        RotateLeft(node);
                    }
                    else
                    {
                        RotateRightLeft(node);
                    }

                    return;
                }

                AVL_node parent = node._PARENT;

                if (parent != null)
                {
                    balance = parent._LEFT == node ? 1 : -1;
                }

                node = parent;
            }
        }
    }

    private AVL_node RotateLeft(AVL_node node)
    {
        AVL_node right = node._RIGHT;
        AVL_node rightLeft = right._LEFT;
        AVL_node parent = node._PARENT;

        right._PARENT = parent;
        right._LEFT = node;
        node._RIGHT = rightLeft;
        node._PARENT = right;

        if (rightLeft != null)
        {
            rightLeft._PARENT = node;
        }

        if (node == _ROOT)
        {
            _ROOT = right;
        }
        else if (parent._RIGHT == node)
        {
            parent._RIGHT = right;
        }
        else
        {
            parent._LEFT = right;
        }

        right.balance++;
        node.balance = -right.balance;

        return right;
    }

    private AVL_node RotateRight(AVL_node node)
    {
        AVL_node left = node._LEFT;
        AVL_node leftRight = left._RIGHT;
        AVL_node parent = node._PARENT;

        left._PARENT = parent;
        left._RIGHT = node;
        node._LEFT = leftRight;
        node._PARENT = left;

        if (leftRight != null)
        {
            leftRight._PARENT = node;
        }

        if (node == _ROOT)
        {
            _ROOT = left;
        }
        else if (parent._LEFT == node)
        {
            parent._LEFT = left;
        }
        else
        {
            parent._RIGHT = left;
        }

        left.balance--;
        node.balance = -left.balance;

        return left;
    }

    private AVL_node RotateLeftRight(AVL_node node)
    {
        AVL_node left = node._LEFT;
        AVL_node leftRight = left._RIGHT;
        AVL_node parent = node._PARENT;
        AVL_node leftRightRight = leftRight._RIGHT;
        AVL_node leftRightLeft = leftRight._LEFT;

        leftRight._PARENT = parent;
        node._LEFT = leftRightRight;
        left._RIGHT = leftRightLeft;
        leftRight._LEFT = left;
        leftRight._RIGHT = node;
        left._PARENT = leftRight;
        node._PARENT = leftRight;

        if (leftRightRight != null)
        {
            leftRightRight._PARENT = node;
        }

        if (leftRightLeft != null)
        {
            leftRightLeft._PARENT = left;
        }

        if (node == _ROOT)
        {
            _ROOT = leftRight;
        }
        else if (parent._LEFT == node)
        {
            parent._LEFT = leftRight;
        }
        else
        {
            parent._RIGHT = leftRight;
        }

        if (leftRight.balance == -1)
        {
            node.balance = 0;
            left.balance = 1;
        }
        else if (leftRight.balance == 0)
        {
            node.balance = 0;
            left.balance = 0;
        }
        else
        {
            node.balance = -1;
            left.balance = 0;
        }

        leftRight.balance = 0;

        return leftRight;
    }

    private AVL_node RotateRightLeft(AVL_node node)
    {
        AVL_node right = node._RIGHT;
        AVL_node rightLeft = right._LEFT;
        AVL_node parent = node._PARENT;
        AVL_node rightLeftLeft = rightLeft._LEFT;
        AVL_node rightLeftRight = rightLeft._RIGHT;

        rightLeft._PARENT = parent;
        node._RIGHT = rightLeftLeft;
        right._LEFT = rightLeftRight;
        rightLeft._RIGHT = right;
        rightLeft._LEFT = node;
        right._PARENT = rightLeft;
        node._PARENT = rightLeft;

        if (rightLeftLeft != null)
        {
            rightLeftLeft._PARENT = node;
        }

        if (rightLeftRight != null)
        {
            rightLeftRight._PARENT = right;
        }

        if (node == _ROOT)
        {
            _ROOT = rightLeft;
        }
        else if (parent._RIGHT == node)
        {
            parent._RIGHT = rightLeft;
        }
        else
        {
            parent._LEFT = rightLeft;
        }

        if (rightLeft.balance == 1)
        {
            node.balance = 0;
            right.balance = -1;
        }
        else if (rightLeft.balance == 0)
        {
            node.balance = 0;
            right.balance = 0;
        }
        else
        {
            node.balance = 1;
            right.balance = 0;
        }

        rightLeft.balance = 0;

        return rightLeft;
    }


    public ebermejo_Vertex search(double key)
    {
        AVL_node node = _ROOT;
        while(node != null)
        {
            if(key < node.key)
            {
                node = node._LEFT;
            }
            else if(key > node.key)
            {
                node = node._RIGHT; 
            }
            else
            {
                return node.data; 
            }
        }
        return null;
    }

    public void clear()
    {
        _ROOT = null; 
    }
}

