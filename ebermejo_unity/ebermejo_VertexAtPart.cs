﻿using System.Collections.Generic;
using UnityEngine;

public enum _parts { lowerlegs, upperlegs, waist, belly, chest, neck, head, upperarms, lowerarms };
public class ebermejo_VertexAtPart
{
    public _parts part;

    public bool boundry;

    public List<Vector3> weights;

    public ebermejo_VertexAtPart(_parts npart, bool nboundry)
    {
        part = npart;

        boundry = nboundry;
    }

    public void addWeightX(Vector3 w)
    {
        weights.Add(w);
    }
}