﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public class GlobalControl : MonoBehaviour
{
    public string fem_model_path;
    public string male_model_path;

    private Mesh female_mesh;
    private Mesh male_mesh;

    private AVL_tree female_tree;
    private AVL_tree male_tree; 

    public MeshFilter active;

    /// <summary>
    /// CONTROL
    /// BOOL GENDER -> ACTIVE MODEL
    bool gender;
    /// BOOL REASIGN -> CHANGE MODEL
    bool reasign;
    /// BOOL RECALCULATE -> RECALCULATE NORMALS
    bool recalculate;
    /// </summary>

    // Use this for initialization
    void Start()
    {
        gender = true;
        reasign = true;
        recalculate = true; 

        female_tree = new AVL_tree();
        male_tree = new AVL_tree();

        female_mesh = readOBJ(fem_model_path, female_tree);
        male_mesh = readOBJ(male_model_path, male_tree);
    }

    // Update is called once per frame
    void Update()
    {
        if(reasign)
        {
            reasign = false; 
            if(gender)
            {
                active.mesh = female_mesh;
            }
            else
            {
                active.mesh = male_mesh;
            }
            recalculateMeshParams(); 
            recalculate = false; 
        }

        if (recalculate) recalculateMeshParams();
    }







    protected void recalculateMeshParams()
    {
        active.mesh.RecalculateNormals();
        active.mesh.RecalculateTangents();
        active.mesh.RecalculateBounds();
    }


    public Mesh readOBJ(string path, AVL_tree tree)
    {
        Mesh toRet = new Mesh();
        List<Vector3> vertices = new List<Vector3>();
        List<int> faces = new List<int>();


        StreamReader sr = new StreamReader(path);
        int index = 0;
        while (!sr.EndOfStream)
        {
            string l = sr.ReadLine();
            if (l[0] == 'v')
            {
                l = l.Substring(2, l.Length - 2);

                string sx = l.Substring(0, l.IndexOf(' '));
                l = l.Substring(l.IndexOf(' ') + 1, l.Length - l.IndexOf(' ') - 1);

                string sy = l.Substring(0, l.IndexOf(' '));
                l = l.Substring(l.IndexOf(' ') + 1, l.Length - l.IndexOf(' ') - 1);

                string sz = l;

                float x, y, z;
                x = Convert.ToSingle(sx);
                y = Convert.ToSingle(sy);
                z = Convert.ToSingle(sz);

                Vector3 pos = new Vector3(x, y, z);

                ebermejo_Vertex v = new ebermejo_Vertex(index, pos);
                vertices.Add(new Vector3(x, y, z));

                tree.insertNode(v);

                index++;
            }
        }
        sr.Close();

        sr = new StreamReader(path);
        while (!sr.EndOfStream)
        {
            string l = sr.ReadLine();
            if (l[0] == 'f')
            {
                l = l.Substring(2, l.Length - 2);
                string s_1 = l.Substring(0, l.IndexOf(' '));
                l = l.Substring(l.IndexOf(' ') + 1, l.Length - l.IndexOf(' ') - 1);

                string s_2 = l.Substring(0, l.IndexOf(' '));
                l = l.Substring(l.IndexOf(' ') + 1, l.Length - l.IndexOf(' ') - 1);

                string s_3 = l;

                int i1, i2, i3;
                i1 = Convert.ToInt32(s_1) - 1;
                i2 = Convert.ToInt32(s_2) - 1;
                i3 = Convert.ToInt32(s_3) - 1;

                ebermejo_Vertex v1, v2, v3;
                v1 = tree.search(i1);
                v2 = tree.search(i2);
                v3 = tree.search(i3);

                v1.addComunicated(v2);
                v1.addComunicated(v3);

                v2.addComunicated(v1);
                v2.addComunicated(v3);

                v3.addComunicated(v1);
                v3.addComunicated(v2);

                faces.Add(i1);
                faces.Add(i2);
                faces.Add(i3);
            }
        }

        Vector3[] verts = new Vector3[vertices.Count];
        vertices.CopyTo(verts);
        toRet.vertices = verts;

        int[] fa = new int[faces.Count];
        faces.CopyTo(fa);
        toRet.triangles = fa;

        return toRet;
    }

    protected void readTree(AVL_node root)
    {
        if (root != null)
        {
            if (root._LEFT != null) readTree(root._LEFT);
            print(root.key);
            if (root._RIGHT != null) readTree(root._RIGHT);
        }


    }
}
