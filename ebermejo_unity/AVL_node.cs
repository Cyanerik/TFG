﻿public class AVL_node
{
    public AVL_node _RIGHT;
    public AVL_node _LEFT;
    public AVL_node _PARENT;

    public double key;
    public ebermejo_Vertex data;
    public int balance;
    
    public AVL_node()
    {
        _RIGHT = null;
        _LEFT = null;
        _PARENT = null;

        key = 0;
        data = null;
        balance = 0; 
    }

}