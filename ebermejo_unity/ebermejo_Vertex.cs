﻿using System.Collections.Generic;
using UnityEngine;

public class ebermejo_Vertex
{
    public Vector3 pos;

    public int _INDEX;

    public List<ebermejo_Vertex> comunicated;
    public List<ebermejo_VertexAtPart> parts;

    //JUST FOR SMOOTHING METHOD
    public string branchName; 
    //////////////////////////


    public ebermejo_Vertex(int nindex, Vector3 npos)
    {
        _INDEX = nindex;
        pos = npos;
        comunicated = new List<ebermejo_Vertex>();
        parts = new List<ebermejo_VertexAtPart>();
    }

    public void addComunicated(ebermejo_Vertex v)
    {
        comunicated.Add(v);
    }

    public void addPart(ebermejo_VertexAtPart p)
    {
        parts.Add(p);
    }
}