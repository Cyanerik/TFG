﻿Please read carefully the following terms and conditions and any accompanying documentation before you download and/or use the SMPL body model and software, (the "Model"), including 3D meshes, blend weights blend shapes, textures, software, scripts, and animations. By downloading and/or using the Model, you acknowledge that you have read these terms and conditions, understand them, and agree to be bound by them. If you do not agree with these terms and conditions, you must not download and/or use the Model.

Ownership
This Software has been developed and is owned by and proprietary material by Erik Tordera Bermejo

License Grant
Erik Tordera grants you a non-exclusive, non-transferable, free of charge right:

This software is based on the SMPL model, in order to modify it you will have to get a SMPL license. 